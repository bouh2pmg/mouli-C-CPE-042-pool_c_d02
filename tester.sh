#!/bin/bash

if [ $# -lt 1 ]
then
    echo -e "[ERREUR] $0 : pas assez d'arguments"
    echo -e "USAGE : $0 exercice [-c]"
    exit 2
fi

exercise=ex_$1
binary=$exercise.sh
output="output.test"

if [ ! -e $exercise ]
then
    echo "KO: exercise [$binary] not found"
    exit 2
fi
case "$exercise" in
    "ex_10")
	if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
	then
	    touch tests/ex_10/toto~
	    touch tests/ex_10/test1/toto~
	    touch tests/ex_10/test2/toto~
	    touch tests/ex_10/test3~/toto1~
	    chmod -R 777 tests/ex_10/
	    ./tests/$binary $exercise &> "./tests/${exercise}_REF"
	else
	    touch tests/ex_10/toto~
	    touch tests/ex_10/test1/toto~
	    touch tests/ex_10/test2/toto~
	    touch tests/ex_10/test3~/toto1~
	    chmod -R 777 tests/ex_10/
	    sudo -u student ./tests/$binary $exercise &> output.test
	    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))
	    
	    if [ $? -ne 0 ]; then
		echo "KO: check your traces below this line..."
		echo "$res"
	    else
		echo "OK"
	    fi
	fi
    ;;
    *)
	if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
	then
	    cd ./tests && ./$binary ./$exercise &> "./${exercise}_REF"
	else
	    sudo -u student ./tests/$binary ./$exercise &> output.test
	    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))
	    
	    if [ $? -ne 0 ]; then
		echo "KO: check your traces below this line..."
		echo "$res"
	    else
		echo "OK"
	    fi
	fi
    ;;
esac
